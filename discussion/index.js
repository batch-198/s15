// Javascript Statements
/* 
	>> set of instructions that we tell the
	computer/machine to perform
	>> JS statements usually ends with semicolon (;), to locate where the statements end
		*/ 


console.log("Hello once more");

// Comments
// one line comment (ctrl + /)
/* multiline comment (ctrl + shift + /*/

// Vairables
	// used for containing data
/* 
	Syntax:
		let / const variableName;
*/

// Initializing Variables
let hello;
console.log(hello); 
// ^ Result: undefined

// console.log(x);
// let x;
// ^ Result Error

// Declaration of Variables
let firstName = "Jin";
let product_description = "lorem ipsum";
let product_id = "250000ea1000";

/* 
	Syntax:
		let / const variableName = value;
*/

let productName = "desktop computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const pi = 3.14;
console.log(pi);

/*let >> there are changes the the value will change/value can be changed

 const >> value is constant, and will not change throughout the code */

// Reassigning variable
/* 
	Syntax:
		variableName = new value;
*/

productName = "Laptop"
console.log(productName);

let friend = "Kate";
friend = "Chance";
console.log(friend);

/*let friend = "Jane";
console.log(friend);*/
// ^ Result: Error "identifier friend has already been declared"

/*pi = 3.1;
console.log(pi);*/
// ^ result: Error "assignment to constant variable"

// Reassigning vs. Initializing variable
let supplier;
supplier = "Jane Smith Tradings"
console.log(supplier);

supplier = "Zuitt Store"
console.log(supplier);

// var vs. let/const
a = 5;
console.log(a);
var a;

// var has a hoisting behavior, so it gets read by the machine first

// let/ const local/ global scope

let outerVariable = "hello";
// {
// 	let innerVariable = "hello again";
// }

console.log(outerVariable);
// ^Result: hello

// console.log(innerVariable);
// ^Result: innerVariable is not defined

/*if global variable - it can be used all throughout the code

local variable - can only be used in their codeblock*/


// Multiple Variable Declaration

let productCode = 'DC017', productBrand = "Dell";
console.log(productCode, productBrand);
// ^ Result: DC017, Dell

/*const let = 'hello';
console.log(let);*/
// ^ Result: Error (reserved name)

// Data Types
	// String - series of characters to form a text
let country = "Philippines";
let city = "Manila City";

console.log(city, country);
// ^ Result: Manila City Philippines

	// Concatenating Strings
let fullAddress = city + ',' + ' ' + country;
console.log(fullAddress);
// ^ Result: Manila City, Philippines

let myName = "Mika";
let greeting = "Hi I am " + myName;
console.log(greeting);

// Escape Character (\)
	// "\n - it will create a new line"

let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

// Numbers

// Integers and Whole Numbers
let headcount = 26;
console.log(headcount); 
// ^Result: 26

// Decimal Numbers
let grade = 74.9;
console.log(grade);
// ^ Result: 74.9

// Exponential Notation
let planetDistance = 2e10;
console.log(planetDistance);
// ^ Result: 20000000000

// Combining Text and String
console.log("John's grade last quarter is " + grade);

// Boolean - either true or false value
let isMarried = false;
let isSingle = true;
console.log("isMarried " + isMarried);
console.log("isSingle " + isSingle);

// Arrays
/*
	Syntax:
		let/const arrayName = [elementA, elementB,..];
*/

// Element means individual data of Array

//similar data types
let grades = [98.7, 77.3, 90.8, 88.4];
console.log(grades);

const anime = ["BNHA", "AOT", "SxF", "KNY"];
console.log(anime);

// different data types
let random = ["JK", 24, true];
console.log(random);

// Object Data Type
/*
	Syntax:
		let/const objectName = {
	propertyA : valueA,
	propertyB : valueB
		}
*/

// Property acts as a label under the variableName

let person = {
	fullName: "Edward Scissorhands",
	age: 35,
	isMarried: false,
	contact: ["09123456789", "8123 4567"],
	address: {
		houseNumber: '345',
		city: 'Manila'
	}

};
console.log(person);


const myGrades = {
	firstGrading: 98.7,
	secondGrading: 77.3,
	thirdGrading: 88.6,
	fourthGrading: 74.9
};

console.log(myGrades);

// typeof operator - checks the type of the specified variableName
console.log(typeof myGrades);
// ^ Result: Object

console.log(typeof grades);
//  ^ Result: Object (Special Type of Object)

console.log(typeof grade);
//  ^ Result: Number

// anime = ['One Punch Man'];
// console.log(anime); 
// ^ Result: Error (assignment to constant)

anime[0]= ['One Punch Man'];
console.log(anime);

// Elements inside the special type of object can be changed even if it's const

// Null
let spouse = null;
console.log(spouse);
// ^ Result: null

// Undefined - as if placeholder
let y;
console.log(y);
// ^ Result: undefined